#include "filelistmodel.h"
#include <QDirIterator>
#include <QDir>
#include <QDebug>
#include "utility.h"
#include <QBrush>

const quint16 FileListModel::MAX_FILE_SIZE = 255;

FileListModel::FileListModel(QObject* tParent)
    : QAbstractListModel (tParent),
      mSourceFiles(QStringList()),
      mRoot(""),
      mDestination("")
{

}

void FileListModel::parseDirectory(QString tAbsolutePath) {
    beginResetModel();
    mSourceFiles.clear();
    QDir root(tAbsolutePath);
    QDirIterator it(tAbsolutePath, QDir::Files, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        mSourceFiles << it.next().remove(0, tAbsolutePath.size());
    }
    endResetModel();
    qDebug() << "Done scanning";
}

void FileListModel::setDestination(QString tAbsolutePath) {
    mDestination = tAbsolutePath;
}

int FileListModel::getSizeAtDestination(const QString &tFileName) const {
    return  mDestination.size() + tFileName.size();
}

const QString &FileListModel::getSourceDir() const
{
    return mRoot;
}

const QString &FileListModel::getDestinationDir() const
{
    return mDestination;
}

int FileListModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return mSourceFiles.size();
}

QVariant FileListModel::data(const QModelIndex &index, int role) const {
    if (role == Qt::DisplayRole) {
        return QString("%1, Size: %2")
                    .arg(mSourceFiles[index.row()])
                    .arg(mDestination.size() + mSourceFiles[index.row()].size() + 1);
    }
    if(role == Qt::ForegroundRole) {
        if(getSizeAtDestination(mSourceFiles[index.row()]) >= MAX_FILE_SIZE) {
            return Utility::getRedBrush();
        }
        else {
            return Utility::getBlackBrush();
        }
    }

    return QVariant();
}
