#ifndef FILELISTMODEL_H
#define FILELISTMODEL_H

#include <QAbstractListModel>

class FileListModel : public QAbstractListModel
{
public:
    FileListModel(QObject* tParent = Q_NULLPTR);
    void parseDirectory(QString tAbsolutePath);
    void setDestination(QString tAbsolutePath);
    int getSizeAtDestination(const QString& tFileName) const;
    const QString& getSourceDir() const;
    const QString& getDestinationDir() const;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

private:
    QStringList mSourceFiles;
    QString mRoot;
    QString mDestination;

    static const quint16 MAX_FILE_SIZE;

};

#endif // FILELISTMODEL_H
