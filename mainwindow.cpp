#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDebug>

#include "filelistmodel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mFileListModel(new FileListModel(this))
{
    ui->setupUi(this);
    connect(ui->actionDestination, &QAction::triggered, this, &MainWindow::setDestinationClicked);
    connect(ui->actionSource, &QAction::triggered, this, &MainWindow::setSourceClicked);
    connect(ui->actionFilter, &QAction::triggered, this, &MainWindow::setFilter);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::quit);
    mFilterProxyModel.setSourceModel(mFileListModel);
    mFilterProxyModel.setFilterRegularExpression("Size: ([3-9]\\d{2,}|26\\d|25[6-9])$");
    ui->listView->setModel(mFileListModel);
    ui->actionSource->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mFileListModel;
}

void MainWindow::setDestinationClicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Destination Directory"),
                                                 "C:/",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    mFileListModel->setDestination(dir);
    ui->destinationLabel->setText("Destination : " + dir);
    ui->actionSource->setEnabled(!dir.isEmpty());
}

void MainWindow::setSourceClicked()
{
    if(mFileListModel->getDestinationDir().isEmpty()) {
        qWarning() << "Set a destination first";
        return;
    }

    QString dir = QFileDialog::getExistingDirectory(this, tr("Source Directory"),
                                                 "C:/",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    qDebug() << dir;
    mFileListModel->parseDirectory(dir);
    ui->sourceLabel->setText("Source : " + dir);
}

void MainWindow::setFilter(bool tState)
{
    if(tState) {
        ui->listView->setModel(&mFilterProxyModel);
    }
    else {
        ui->listView->setModel(mFileListModel);
    }
}

void MainWindow::quit()
{
    QApplication::quit();
}
