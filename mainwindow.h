#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QSortFilterProxyModel>

namespace Ui {
class MainWindow;
}

class FileListModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void targetDirectoryChanged();
    void sourceDirectoryChanged();

public slots:
    void setDestinationClicked();
    void setSourceClicked();
    void setFilter(bool tState);
    void quit();

private:
    Ui::MainWindow *ui;
    QSortFilterProxyModel mFilterProxyModel;
    FileListModel* mFileListModel;
};

#endif // MAINWINDOW_H
