#include "utility.h"
#include <QBrush>
#include <QColor>

const QBrush &Utility::getBlackBrush()
{
    static QBrush brush(QColor(0,0,0));
    return brush;
}

const QBrush &Utility::getRedBrush()
{
    static QBrush brush(QColor(255,0,0));
    return brush;
}

