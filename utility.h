#ifndef UTILITY_H
#define UTILITY_H

class QBrush;

class Utility
{
public:
    static const QBrush& getBlackBrush();
    static const QBrush& getRedBrush();

private:
    Utility() = delete;
    Utility(const Utility&) = delete;
};

#endif // UTILITY_H
